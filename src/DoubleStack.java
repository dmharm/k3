import java.util.*;
import java.util.LinkedList;

public class DoubleStack {


   public static void main (String[] argum) {
   }

   private final LinkedList<Double> m = new LinkedList<>();

   DoubleStack() {
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      DoubleStack clone = new DoubleStack();
      for (int i = 0; i <m.size(); i++) {
         clone.m.addLast(m.get(i));
      }
      return clone;
   }

   public boolean stEmpty() {
      return m.isEmpty();
   }

   public void push (double a) {
      m.addFirst(a);
   }

   public double pop() {
      if (m.isEmpty()) {
         throw new RuntimeException("No elements");
      }
      double element = m.getFirst();
      m.removeFirst();
      return element;
   }

   public void op (String s) throws RuntimeException {


      try {
         double a = m.pop();
         double b = m.pop();

         if ("+".equals(s)) {
            m.push(b + a);
         } else if ("-".equals(s)) {
            m.push(b - a);
         } else if ("*".equals(s)) {
            m.push(b * a);
         } else if ("/".equals(s)) {
            m.push(b / a);
         } else {
            System.out.printf("'%s' can not be operator, '+', '-', '*' or '/' just can be", s);
         }
      } catch (RuntimeException e) {
         throw new RuntimeException(String.format("Can not do operation %s", s));
      }
   }
   public void rot() {
      if (m.size() < 3) {
         throw new RuntimeException("Not enough numbers for ROT");
      }
      double first = pop();
      double second = pop();
      double third = pop();

      push(second);
      push(first);
      push(third);

   }

   public void swap() {
      if (m.size() < 2) {
         throw new RuntimeException("Not enough numbers for SWAP");
      }
      double first = pop();
      double second = pop();

      push(first);
      push(second);
   }

   public void dup() {
      if (m.size() < 1) {
         throw new RuntimeException("Not enough numbers for DUP");
      }
      double first = pop();
      push(first);
      push(first);


   }



   public double tos() {
      if (m.isEmpty()) {
         throw new RuntimeException("Stack is empty!");
      }
      return m.getFirst();
   }


   @Override
   public boolean equals (Object o) {

      DoubleStack ds = (DoubleStack) o;

      if (m.size() != ds.m.size())
         return false;


      for (int i = 0; i < m.size(); i++)
         if (!ds.m.get(i).equals(m.get(i))){
            return false;
         }
      return true;


   }

   @Override
   public String toString() {
      StringBuilder b = new StringBuilder();

      for (int i= (m.size()-1); i >= 0; i--)
         b.append(m.get(i));

      return b.toString();
   }

   public static double interpret (String pol) {

      if (pol.equals("")) throw new RuntimeException("Empty expression");

      StringTokenizer tokens = new StringTokenizer(pol," \t");

      DoubleStack doubleStack = new DoubleStack();

      int i = 1;
      int tokenCount = tokens.countTokens();
      while (tokens.hasMoreElements()) {
         String token = (String) tokens.nextElement();

         if (token.equals("ROT")) {
            doubleStack.rot();
         } else if (token.equals("SWAP")) {
            doubleStack.swap();
         } else if (token.equals("DUP")) {
            doubleStack.dup();
         }


         else if (token.equals("-") || token.equals("+") || token.equals("/") || token.equals("*"))
         {
            try {
               doubleStack.op(token);
            } catch (RuntimeException e) {
               throw new RuntimeException(String.format("Can not perform %s in expression \"%s\"", token, pol));
            }
         }
         else {

            if (tokenCount == i && i > 2)
               throw new RuntimeException("Not possible" + pol);

            double temp;
            try {
               temp = Double.parseDouble(token);
            } catch (RuntimeException e) {
               throw new RuntimeException("Illegal symbol " + token + " in expression " + pol);
            }

            doubleStack.push(temp);
         }
         i++;
      }

      if (doubleStack.m.size() > 1) throw new RuntimeException("Too many numbers");

      return doubleStack.tos();
   }}